package project.com.atmmachine.ut

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.mock.web.MockHttpServletRequest
import project.com.atmmachine.repository.AccountRepository
import project.com.atmmachine.service.AccountService
import spock.lang.Specification
import spock.lang.Unroll

import static project.com.atmmachine.generator.AccountGenerator.anAccount

class AccountServiceSpec extends Specification {

    def accountRepository = Mock(AccountRepository)
    def accountService = new AccountService(accountRepository)

    @Unroll
    def "accountAction"() {
        given:
        def request = new MockHttpServletRequest()
        request.session.setAttribute("session.attribute.card.number", sessionCardNumber)
        request.session.setAttribute("session.attribute.pin", sessionPin)

        when:
        def result = accountService.accountAction(action, number, request)

        then:
        0 * _
        result == res

        and:
        findByIdCalls * accountRepository.findById(cardNumber) >> findById
        saveCalls * accountRepository.save(anAccount(amount: anAccount().getAmount() - number))

        where:
        action         | number | cardNumber | sessionCardNumber | sessionPin | findByIdCalls | findById                 | saveCalls || res
        "readCard"     | 1357l  | 1357l      | null              | null       | 1             | Optional.empty()         | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "readCard"     | 1357l  | 1357l      | 1357l             | null       | 1             | Optional.of(anAccount()) | 0         || new ResponseEntity("Bine ati venit!", HttpStatus.OK)
        "readCard"     | 1357l  | 1357l      | null              | null       | 1             | Optional.of(anAccount()) | 0         || new ResponseEntity("Bine ati venit!", HttpStatus.OK)
        "readPin"      | 1234   | null       | null              | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "readPin"      | 1234   | 1357l      | 1357l             | null       | 1             | Optional.of(anAccount()) | 0         || new ResponseEntity("Pin valid.", HttpStatus.OK)
        "viewAmount"   | 1234   | 1357l      | null              | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "viewAmount"   | 1234   | 1357l      | null              | 1234l      | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "viewAmount"   | 1234   | 1357l      | 1357l             | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "viewAmount"   | 1234   | 1357l      | 1357l             | 1234l      | 1             | Optional.empty()         | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "viewAmount"   | 1234   | 1357l      | 1357l             | 1234l      | 1             | Optional.of(anAccount()) | 0         || new ResponseEntity<>(567.34d, HttpStatus.OK)
        "removeAmount" | 1234   | 1357l      | null              | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "removeAmount" | 1234   | 1357l      | null              | 1234l      | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "removeAmount" | 1234   | 1357l      | 1357l             | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST)
        "removeAmount" | 600    | 1357l      | 1357l             | 1234l      | 1             | Optional.of(anAccount()) | 0         || new ResponseEntity<>("Fonduri insuficiente!", HttpStatus.BAD_REQUEST)
        "removeAmount" | 300    | 1357l      | 1357l             | 1234l      | 1             | Optional.of(anAccount()) | 1         || new ResponseEntity<>("Tranzactia a fost realizata cu succes. Va rugam asteptati suma de bani.", HttpStatus.OK)
        "ejectCard"    | 1357   | 1357l      | null              | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity<>("Sa aveti o zi buna!", HttpStatus.OK)
        "ejectCard"    | 1357   | 1357l      | null              | 1234l      | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity<>("Sa aveti o zi buna!", HttpStatus.OK)
        "ejectCard"    | 1357   | 1357l      | 1357l             | null       | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity<>("Sa aveti o zi buna!", HttpStatus.OK)
        "ejectCard"    | 1357   | 1357l      | 1357l             | 1234l      | 0             | Optional.of(anAccount()) | 0         || new ResponseEntity<>("Sa aveti o zi buna!", HttpStatus.OK)
    }
}
