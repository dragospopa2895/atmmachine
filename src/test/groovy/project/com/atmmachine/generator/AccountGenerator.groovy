package project.com.atmmachine.generator

import project.com.atmmachine.entity.Account

class AccountGenerator {
    static anAccount(override = [:]) {
        Map values = [
                cardNumber: 1357,
                firstName : "Dumitrescu",
                lastName  : "Tudor",
                pin       : 1234,
                amount    : 567.34
        ]
        values << override

        return Account.newInstance(values)
    }
}
