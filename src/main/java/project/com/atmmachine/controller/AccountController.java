package project.com.atmmachine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.com.atmmachine.service.AccountService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping
@ResponseBody
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/{action}/{cardNumber}", method = RequestMethod.GET)
    public Object getAccountByCardNumber(@PathVariable final String action, @PathVariable final Long cardNumber,
                                         final HttpServletRequest request) {
        return accountService.accountAction(action, cardNumber, request);
    }

}
