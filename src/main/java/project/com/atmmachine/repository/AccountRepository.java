package project.com.atmmachine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.com.atmmachine.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
}
