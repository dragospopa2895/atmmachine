package project.com.atmmachine;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import project.com.atmmachine.entity.Account;
import project.com.atmmachine.repository.AccountRepository;

@SpringBootApplication
public class AtmmachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtmmachineApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(final AccountRepository accountRepository) {
		return (args) -> {
			accountRepository.save(new Account(1357l, "Dumitrescu", "Vlad", 1234l, 3567.32));
			accountRepository.save(new Account(2468l, "Albut", "Gheorghe", 1234l, 400d));
			accountRepository.save(new Account(9876l, "Marin", "Ilie", 1234l, 505.5));
			accountRepository.save(new Account(1234l, "Popescu", "Lucian", 1234l, 2345.657));
		};
	}

}
