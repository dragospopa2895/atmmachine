package project.com.atmmachine.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import project.com.atmmachine.entity.Account;
import project.com.atmmachine.repository.AccountRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class AccountService {

    private static final String CARD_NUMBER = "session.attribute.card.number";
    private static final String PIN = "session.attribute.pin";
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Object accountAction(final String action, final Long number, final HttpServletRequest request) {
        Object objectToBeReturn = new Object();
        switch(action){
            case "readCard":
                objectToBeReturn = existAccountWithCardNumber(number, request);
                break;
            case "readPin":
                objectToBeReturn = validPin(number, request);
                break;
            case "viewAmount":
                objectToBeReturn = viewAmount(request);
                break;
            case "removeAmount":
                objectToBeReturn = removeAmount(number, request);
                break;
            case "ejectCard":
                objectToBeReturn = ejectCard(request);
                break;
        }

        return objectToBeReturn;
    }

    private Object existAccountWithCardNumber(final Long cardNumber, final HttpServletRequest request) {
        LOGGER.info("[readCard] Citire card {}", cardNumber);
        final Optional<Account> accountOptional = accountRepository.findById(cardNumber);
        if(accountOptional.isPresent()){
            if (request.getSession().getAttribute(CARD_NUMBER) == null) {
                request.getSession().setAttribute(CARD_NUMBER, cardNumber);
            }
            return new ResponseEntity<>("Bine ati venit!", HttpStatus.OK);
        }
        LOGGER.error("Eroare citire card {}", cardNumber);
        return new ResponseEntity<>("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST);
    }

    private Object validPin(final Long pin, final HttpServletRequest request) {
        LOGGER.info("[validPin] Validare pin {}", pin);
        Object cardNumber = request.getSession().getAttribute(CARD_NUMBER);
        if(cardNumber != null) {
            LOGGER.info("[validPin] Validare pin {} al cardului cu numarul {}", pin, cardNumber);
            if (pin != null) {
                final Optional<Account> accountOptional = accountRepository.findById((long) cardNumber);
                if (accountOptional.isPresent() && pin.equals(accountOptional.get().getPin())) {
                    if (request.getSession().getAttribute(PIN) == null) {
                        request.getSession().setAttribute(PIN, pin);
                    }
                    return new ResponseEntity<>("Pin valid.", HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST);
    }

    private Object viewAmount(final HttpServletRequest request) {
        LOGGER.info("[viewAmount] Verificare suma cont");
        Object cardNumber = request.getSession().getAttribute(CARD_NUMBER);
        Object pin = request.getSession().getAttribute(PIN);
        if(cardNumber != null && pin != null) {
            final Optional<Account> accountOptional = accountRepository.findById((long)cardNumber);
            if(accountOptional.isPresent()) {
                return new ResponseEntity<>(accountOptional.get().getAmount(), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST);
    }

    private Object removeAmount(final Long amount, final HttpServletRequest request) {
        LOGGER.info("[removeAmount] Scoatere suma cont");
        Object cardNumber = request.getSession().getAttribute(CARD_NUMBER);
        Object pin = request.getSession().getAttribute(PIN);
        if(cardNumber != null && pin != null) {
            final Optional<Account> accountOptional = accountRepository.findById((long) cardNumber);
            if(accountOptional.isPresent()) {
                Account account = accountOptional.get();
                if(account.getAmount() - amount >= 0) {
                    account.setAmount(account.getAmount()-amount);
                    accountRepository.save(account);
                    return new ResponseEntity<>(
                            "Tranzactia a fost realizata cu succes. Va rugam asteptati suma de bani.", HttpStatus.OK);
                }
                return new ResponseEntity<>("Fonduri insuficiente!", HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>("Cardul nu poate fi recunoscut!", HttpStatus.BAD_REQUEST);
    }

    private Object ejectCard(final HttpServletRequest request) {
        LOGGER.info("[ejectCard] Scoatere card din ATM");
        if (request.getSession().getAttribute(CARD_NUMBER) != null) {
            request.getSession().setAttribute(CARD_NUMBER, null);
        }
        if (request.getSession().getAttribute(PIN) != null) {
            request.getSession().setAttribute(PIN, null);
        }
        return new ResponseEntity<>("Sa aveti o zi buna!", HttpStatus.OK);
    }
}
